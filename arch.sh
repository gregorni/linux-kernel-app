#!/bin/bash

height=20
width=50



install-from-repo () {
    yay -S --noconfirm "$@"
}


update-the-bootloader () {
    sudo grub-mkconfig -o /boot/grub/grub.cfg
}


running_kernel=$(uname -r)
installed_kernels=$(find /boot/vmli* | grep "-" | cut -c 15-)


install-linux-tkg () {
    git clone https://github.com/Frogging-Family/linux-tkg.git
    cd linux-tkg || exit
    # Optional: edit the "customization.cfg" file
    makepkg -si
}

install-xanmod () {

    xanmod_generic="Generic"
    xanmod_lts="LTS"
    xanmod_edge="Edge"
    xanmod_tt="Task Type"
    xanmod_rt="Realtime"

    xanmod_selection=$(whiptail --menu "Choose which Xanmod version you want to install:" "$height" "$width" 10 \
"$xanmod_generic" "" \
"$xanmod_lts" "" \
"$xanmod_edge" "" \
"$xanmod_tt" "" \
"$xanmod_rt" "" 3>&1 1>&2 2>&3)

    case "$xanmod_selection" in
        "$xanmod_generic" )
            xanmod_selection=linux-xanmod
            ;;
        "$xanmod_lts" )
            xanmod_selection=linux-xanmod-lts
            ;;
        "$xanmod_edge" )
            xanmod_selection=linux-xanmod-edge
            ;;
        "$xanmod_tt" )
            xanmod_selection=linux-xanmod-tt
            ;;
        "$xanmod_rt" )
            xanmod_selection=linux-xanmod-rt
            ;;

    esac

    install-from-repo "$xanmod_selection"
}

install-generic () {
    install-from-repo linux
}

install-zen () {
    install-from-repo linux-zen
}

install-lts () {
    install-from-repo linux-lts
}

install-hardened () {
    install-from-repo linux-hardened
}

install-liquorix () {
    install-from-repo linux-lqx
}


choose-kernel () {
    kernel_selection=$(whiptail --menu "Choose the kernel you want to install:" "$height" "$width" 10 \
"Generic" "" \
"LTS" "" \
"Xanmod" "" \
"Liquorix" "" \
"Zen" "" \
"Hardened" "" \
"Linux-tkg" "" 3>&1 1>&2 2>&3)

    echo "$kernel_selection" | tr '[:upper:]' '[:lower:]'
}


open-link () {
    xdg-open "$@" > /dev/null
}


visit-kernel-website () {
    case $(choose-kernel) in
        xanmod )
            open-link https://xanmod.org
            ;;
        liquorix )
            open-link https://liquorix.net
            ;;
        generic | lts)
            open-link https://kernel.org
            ;;
        hardened )
            open-link https://github.com/anthraxx/linux-hardened
            ;;
        zen )
            open-link https://github.com/zen-kernel/zen-kernel
            ;;
        linux-tkg )
            open-link https://github.com/Frogging-Family/linux-tkg
            ;;
    esac
}

display-kernel-info () {
    whiptail --msgbox "Running kernel: $running_kernel \n\nInstalled kernels:\n$installed_kernels" "$height" "$width"
}

check-latest-kernel-version () {
	latest_kernel_version=$(curl -s https://kernel.org/ | grep -A 1 "latest_button" | grep -o -P ".{0,7}.tar.xz" | sed 's/-//' | sed 's/.......$//')
	whiptail --msgbox "The latest stable kernel version listed on https://kernel.org is:

$latest_kernel_version" "$height" "$width"
}

main-menu () {
    while true; do
        kernel_info="Get information about installed kernels"
        install_kernel="Install a kernel"
        open_website="Visit a kernels website"
        check_latest_kernel_version="Check the latest kernel version"
        quit="Quit"
        

        selected_action=$(whiptail --menu "What do you want to do?" "$height" "$width" 10 \
"$kernel_info" "" \
"$install_kernel" "" \
"$open_website" "" \
"$check_latest_kernel_version" "" \
"$quit" "" 3>&1 1>&2 2>&3)

        case "$selected_action" in
            "$kernel_info" )
                display-kernel-info
                ;;
            "$install_kernel" )
                install-"$(choose-kernel)"
                update-the-bootloader
                ;;
            "$open_website" )
                visit-kernel-website
                ;;
            "$check_latest_kernel_version" )
            	check-latest-kernel-version
            	;;
            "$quit" | "" )
                exit
                ;;
        esac
    done
}


main-menu
