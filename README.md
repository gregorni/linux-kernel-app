# Linux Kernel App

## About

A set of scripts to install and manage (custom) Linux kernels using whiptail.

## Usage

Download and install the app with these commands:

```bash
git clone https://gitlab.com/gregorni/linux-kernel-app.git
cd linux-kernel-app/
bash install.sh
```

Currently, this app only works on Debian, Arch and their derivatives, but support for other distros (mainly Fedora) is being worked on.

**Note:** On Arch, [yay](https://github.com/Jguer/yay) will be installed if it isn't already.

## Support

If you encounter trouble, feel free to open a discussion under the Issues section.

## License

This project is licensed under GNU GPLv3, so you may do anything with it *except* distributing a closed source version.
