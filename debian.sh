#!/bin/bash

height=20
width=50


install-from-repo () {
    sudo apt install -y "$@"
}

running_kernel=$(uname -r)
installed_kernels=$(find /boot/vmli* | grep "-" | cut -c 15-)


install-linux-tkg () {
    git clone https://github.com/Frogging-Family/linux-tkg.git
    cd linux-tkg || exit
    # Optional: edit the "customization.cfg" file
    ./install.sh install
}

install-xanmod () {

    echo 'deb http://deb.xanmod.org releases main' | sudo tee /etc/apt/sources.list.d/xanmod-kernel.list
    wget -qO - https://dl.xanmod.org/gpg.key | sudo apt-key --keyring /etc/apt/trusted.gpg.d/xanmod-kernel.gpg add -
    sudo apt update

    install-from-repo linux-xanmod
}

install-generic () {
    install-from-repo linux-generic
}

install-liquorix () {
    curl 'https://liquorix.net/add-liquorix-repo.sh' | sudo bash
}


choose-kernel () {
    kernel_selection=$(whiptail --menu "Choose the kernel you want to install:" "$height" "$width" 10 \
"Generic" "" \
"Xanmod" "" \
"Liquorix" "" \
"Linux-tkg" "" 3>&1 1>&2 2>&3)

    echo "$kernel_selection" | tr '[:upper:]' '[:lower:]'
}


open-link () {
    xdg-open "$@" > /dev/null
}


visit-kernel-website () {
    case $(choose-kernel) in
      xanmod )
        open-link https://xanmod.org
        ;;
      liquorix )
        open-link https://liquorix.net
        ;;
      generic )
        open-link https://kernel.org
        ;;
      linux-tkg )
        open-link https://github.com/Frogging-Family/linux-tkg
        ;;
    esac
}

display-kernel-info () {
    whiptail --msgbox "Running kernel: $running_kernel \n\nInstalled kernels:\n$installed_kernels" "$height" "$width"
}


check-latest-kernel-version () {
	latest_kernel_version=$(curl -s https://kernel.org/ | grep -A 1 "latest_button" | grep -o -P ".{0,7}.tar.xz" | sed 's/-//' | sed 's/.......$//')
	whiptail --msgbox "The latest stable kernel version listed on https://kernel.org is:

$latest_kernel_version" "$height" "$width"
}

main-menu () {
    while true; do
        kernel_info="Get information about installed kernels"
        install_kernel="Install a kernel"
        open_website="Visit a kernels website"
        check_latest_kernel_version="Check the latest kernel version"
        quit="Quit"

        selected_action=$(whiptail --menu "What do you want to do?" "$height" "$width" 10 \
"$kernel_info" "" \
"$install_kernel" "" \
"$open_website" "" \
"$check_latest_kernel_version" "" \
"$quit" "" 3>&1 1>&2 2>&3)

        case "$selected_action" in
            "$kernel_info" )
                display-kernel-info
                ;;
            "$install_kernel" )
                install-"$(choose-kernel)"
                ;;
            "$open_website" )
                visit-kernel-website
                ;;
            "$check_latest_kernel_version" )
            	check-latest-kernel-version
            	;;
            "$quit" | "" )
                exit
                ;;
        esac
    done
}


main-menu
