#!/bin/bash

current_dir_without_backslash=$(pwd)
current_dir_with_backslash=$(printf "%q\n" "$current_dir_without_backslash")

distro_basis="$(grep ID /etc/os-release)"
case "$distro_basis" in
    *debian* | ubuntu )
        distro_basis=debian

        ;;

#    *fedora* )
#        distro_basis=
#        ;;

    *arch* )
        distro_basis=arch

        if [[ $(which yay) == *yay ]]; then

            sudo pacman -S --needed --noconfirm git base-devel

            yay_answer=$(whiptail --menu "Which yay version do you want to install?" 10 40 3 \
            "yay" "(More up-to-date)" \
            "yay-bin" "(faster install process)" 3>&1 1>&2 2>&3)

            git clone https://aur.archlinux.org/"$yay_answer".git
            cd "$yay_answer" || exit

            makepkg -si
        fi
        ;;

    * )
        echo "Your distro is unsupported."
        ;;
esac

chmod +x ./*.sh
echo "[Desktop Entry]
Encoding=UTF-8
Version=1.0
Type=Application
Terminal=true
Exec=$current_dir_with_backslash/$distro_basis.sh
Name=Kernel App
Comment=Kernel Management App
Icon=$current_dir_without_backslash/tux.png" > ~/.local/share/applications/kernel-app.desktop
